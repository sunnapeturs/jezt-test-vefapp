document.body.innerHTML = '<div id="cardContainer"></div>';
const {getImg, getTitle, makeCard} = require('../main.js');

test('first letter of title is capital', () => {
    var getFirst = function(x){
        return x[0]
    }
    for(let i = 0; getTitle(i); i++){
        expect(getFirst(getTitle(i))).toBe(getFirst(getTitle(i).toUpperCase()))
    }
});
test('titles should always be wrapped in span element', () => {
    for(let i = 0; getTitle(i); i++){
        expect(makeCard(i)).toMatch(/<span>/);
    }
});
test('titles should always end with a dot', () => {
    for(let i = 0; getTitle(i); i++){
        expect(makeCard(i)).toMatch(/./);
    }
});
