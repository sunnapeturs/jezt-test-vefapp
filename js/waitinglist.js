//SWIPE TO DELETE LIBRARY

const waitinglist = document.getElementsByClassName('card-holder');

for (let i = 0; i < waitinglist.length; i++) {
    new Slip(waitinglist[i]);

    waitinglist[i].addEventListener('slip:swipe', function (e) {
        console.log(e.target.parentNode)
        console.log(e.target.parentNode.parentNode)

        e.target.parentNode.parentNode.parentNode.removeChild(e.target.parentNode.parentNode);
    
    });

}

//DELETE WAITINGLIST ITEM ON CLICK

const deleteBtns = document.getElementsByClassName('card-item-delete');
for (i = 0; i < deleteBtns.length; i++) {
    deleteBtns[i].addEventListener('click', (e) => {
        e.target.parentElement.parentElement.removeChild(e.target.parentElement);
    }
    )
}

// EXPAND/MINIMIZE WAITINGLIST CARD

const expandBtns = document.getElementsByClassName('expand-card-btn');
const minimizeBtns = document.getElementsByClassName('minimize-card-btn');
const requestDescription = document.getElementsByClassName('card-txt-dscr');

for (let i = 0; i < expandBtns.length; i++) {
    expandBtns[i].addEventListener("click", (e) => {
        expandBtns[i].style.display = "none"
        minimizeBtns[i].style.display = "block"
        e.target.parentElement.querySelector('.card-txt-dscr').style.display = "block"
    })

    minimizeBtns[i].addEventListener("click", (e) => {

        minimizeBtns[i].style.display = "none"
        expandBtns[i].style.display = "block"
        e.target.parentElement.querySelector('.card-txt-dscr').style.display = "none"
    })
}