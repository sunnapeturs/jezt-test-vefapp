// SIDEBAR
const burger = document.getElementById('nav-menu');
const sidebar = document.getElementById('sidebar');
const closeNav = document.getElementById('nav-close');
const navItemName = document.getElementsByClassName('nav-item-name');

// OPEN SIDEBAR MENU
burger.addEventListener("click", () => {
    sidebar.style.width = "212px";
    burger.style.display = "none";
    closeNav.style.display = "block";
    
    var i;
    for (i = 0; i < navItemName.length; i++) { 

        navItemName[i].style.display = "block";
    }
})

// CLOSE SIDEBAR MENU
closeNav.addEventListener("click", () => {
    sidebar.style.width = "4.5rem";
    burger.style.display = "block";
    closeNav.style.display = "none";
    
    var i;
    for (i = 0; i < navItemName.length; i++) { 

        navItemName[i].style.display = "none";
    }
})

//////////////////////////////////////////////

//Add link to a picture from a fav movie to the array, remeber to put a comme after the previous link, no comme should be for the last link.
var img = [
    "https://gq-images.condecdn.net/image/qG0vGeYLb8R/crop/1020/f/Room-GQ-31Mar17_rex_b.jpg",
    "https://akm-img-a-in.tosshub.com/indiatoday/images/story/201601/cat---facebook-and-storysize_647_011416045855.jpg"
]
var getImg = function(n, images){ 
    return images[n];
}
//Add a title for the movie, remeber to have the first letter uppercase and a dot after the title
var getTitle = function(n){
    var title = [
        "The room.",
        "The cat."
    ]
    return title[n];
}
////////////////////////////////////////////////////////////TEST
var img = [
    "https://gq-images.condecdn.net/image/qG0vGeYLb8R/crop/1020/f/Room-GQ-31Mar17_rex_b.jpg", 
    "https://gq-images.condecdn.net/image/qG0vGeYLb8R/crop/1020/f/Room-GQ-31Mar17_rex_b.jpg",
    "https://akm-img-a-in.tosshub.com/indiatoday/images/story/201601/cat---facebook-and-storysize_647_011416045855.jpg"
]
var getImg = function(n, images){ 
    return images[n];
//Add a title for the movie, remeber to have the first letter uppercase and a dot after the title
var getTitle = function(n){
    var title = [
        "The room.",
        "The room.",
        "The cat."
    ]
    return title[n];
}
////////////////////////////////////////////////////////////
var makeCard = function(n) {
    var cards = `
        <div class="card">
            <span>${getTitle(n)}</span>
            <img src="${getImg(n, img)}">
        </div>
  `
    return cards;
}
var renderCard = function(n){
         document.querySelector("#cardContainer").innerHTML += makeCard(n);
}

var renderCards = function(){
    var i;
    for(i=0; i < img.length;i++){
        renderCard(i);
    }
}
renderCards();
