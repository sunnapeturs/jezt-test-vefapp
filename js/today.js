//FILTER DROPDOWN - TODAY 

const dropDownContent = document.getElementsByClassName('filter-archive-dropdown-content');
  
for(let i =0;i<dropDownContent.length;i++){
    for(let j =0;j<dropDownContent[i].children.length;j++){
        dropDownContent[i].children[j].addEventListener('click',function(){
     this.parentNode.previousElementSibling.innerHTML = this.innerHTML+`<img src="../img/arrow-down.svg" class="arrow-down-icon">`;
        })
    }
}

//ACCORDIAN - TODAY
    
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.parentElement.nextElementSibling;
        if (panel.style.display === "block") {
            panel.style.display = "none";
        } else {
            panel.style.display = "block";
        }
    });
}